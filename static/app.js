'use strict'
Ext.require([
    'Ext.window.Window',
    'Ext.tab.*',
    'Ext.toolbar.Spacer',
    'Ext.layout.container.Card',
    'Ext.layout.container.Border',
    'Ext.window.MessageBox',
    'Ext.layout.container.Border',
    'Ext.menu.Menu',
    'Ext.panel.*',
    'Ext.toolbar.*',
    'Ext.button.*',
    'Ext.container.ButtonGroup',
    'Ext.layout.container.Table',
    'Ext.tip.QuickTipManager'
]);


Ext.define('Persona', {
    extend: 'Ext.data.Model',
    fields: ['id','name','last_name','birthday','register']
});
Ext.define('Register', {
    extend: 'Ext.data.Model',
    fields: ['name','inn','kpp']
});
Ext.onReady(function(){
    var persons_form = Ext.create('Ext.form.Panel', {
    title: 'Simple Form',
    bodyPadding: 5,
    width: 350,

    // The form will submit an AJAX request to this URL when submitted
    url: '/api/v1/persons/',

    // Fields will be arranged vertically, stretched to full width
    layout: 'anchor',
    defaults: {
        anchor: '100%'
    },

    // The fields
    defaultType: 'textfield',
    items: [{
        fieldLabel: 'First Name',
        name: 'first',
        allowBlank: false
    },{
        fieldLabel: 'Last Name',
        name: 'last',
        allowBlank: false
    }],

    // Reset and Submit buttons
    buttons: [{
        text: 'Сброс',
        handler: function() {
            this.up('form').getForm().reset();
        }
    }, {
        text: 'Сохранить',
        formBind: true, //only enabled once the form is valid
        disabled: true,
        handler: function() {
            var form = this.up('form').getForm();
            if (form.isValid()) {
                form.submit({
                    success: function(form, action) {
                       Ext.Msg.alert('Success', action.result.msg);
                    },
                    failure: function(form, action) {
                        Ext.Msg.alert('Failed', action.result.msg);
                    }
                });
            }
        }
    }],
});

var win_for_person_form = Ext.create('Ext.window.Window', {
        width: 500,
        height: 300,
        minWidth: 300,
        minHeight: 200,
        layout: 'fit',
        plain:true,
        modal: true,
        items: [persons_form],
        buttons: []
    });
var store_persons =  Ext.create('Ext.data.ArrayStore', {
        model: 'Persona',
        pageSize: 50,
        proxy: {
            type: 'ajax',
            url: '/api/v1/persons/',
            reader: {
                type: 'json',
                root: 'data'
            }
        },
        autoLoad: true
    });
/*Ext.Ajax.request({
	   	url: '/api/v1/persons/',
	   	method: 'GET',
	   	success: function(response,con){
                  console.log(response)
	   	},
	   	failure: function(response){
	   			console.log("Error:" + transport.responseText);
	   	}
	});
var store_registers = Ext.create('Ext.data.ArrayStore', {
        model: 'Register',
        pageSize: 50,
        proxy: {
            type: 'ajax',
            url: '/api/v1/registers/',
            reader: {
                type: 'json',
                root: 'data'
            }
        },
        autoLoad: true
});
console.log(store_registers.data.getAt(0));*/
//Адский говнокод
var store_registers = []
function getXmlHttp(){
      var xmlhttp;
      try {
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
      } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
      }
      if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
        xmlhttp = new XMLHttpRequest();
      }
      return xmlhttp;
    };
    var xmlhttp = getXmlHttp();
    xmlhttp.open('GET', '/api/v1/registers/', false);
    xmlhttp.send(null);
    if(xmlhttp.status == 200) {
      store_registers = JSON.parse(xmlhttp.responseText);
}

var persons = Ext.create('Ext.grid.Panel', {
        store:store_persons,
        id:'persons',
        columns: [
        {
            text     : 'ID',
            dataIndex: 'id'
        },
        {
            text     : 'Имя',
            dataIndex: 'name'
        },
        {
            text     : 'Фамилия',
            dataIndex: 'last_name'
        },
        {
            text     : 'Дата Рождения',
            dataIndex: 'birthday'
        },
        {
            text     : 'Организация',
            dataIndex: 'register'
        }
        ],
        height: 350,
        width: 650,
        title: 'Физ.Лица.',
        draggable: true,
        resizable: true,
        bbar: Ext.create('Ext.PagingToolbar', {
            store:store_persons
        })
    });
var tree_store = Ext.create('Ext.data.TreeStore', {
    fields:['name','inn', 'kpp'],
    root: {
        text: 'Минестерство',
        expanded: true,
        children: store_registers
    }
});
    var edit_persons = Ext.create('Ext.Button', {
    text: 'Редактировать',
     handler: function() {
        persons_form.show()
    }
});

    var remove_persons = Ext.create('Ext.Button', {
        text: 'Удалить',
        handler: function() {
            alert('You clicked the button!')
    }
});

    var create_persons = Ext.create('Ext.Button', {
        text: 'Добавить',
        handler: function() {
            win_for_person_form.show()
    }
});
var registers = Ext.create('Ext.tree.Panel', {
        store: tree_store,
        width: 200,
        height: 200,
        rootVisible: true,
        columns: [
                {
                    xtype: 'treecolumn',
                    dataIndex: 'name',
                    flex: 1,
                    text: 'Узлы',
                    editor:'textfield'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'inn',
                    text: 'ИНН',
                    editor:'combobox'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'kpp',
                    text: 'КПП',
                    editor:'combobox'
                }
            ]
    });
    var personsWindow=Ext.create('widget.window', {
        height: 400,
        width: 650,
        closable: true,
        constrain: true,
        layout: 'fit',
        items:[persons],
        buttons: [edit_persons,remove_persons,create_persons]
   });

    var registersWindow=Ext.create('widget.window', {
        height: 500,
        width: 650,
        closable: true,
        constrain: true,
        layout: 'fit',
        items:[registers],
        buttons: ['edir']
   });

    var panelStart = new Ext.panel.Panel({
        width: 200,
        height: 200,
        tools: [{
            xtype: 'button',
            text: 'Пуск',
            menu: {
                items: [{
                    text: 'Физ.лица',
                    handler: function() {
                        personsWindow.show();
                    }
                }, {
                    text: 'Учреждения',
                    handler: function() {
                        registersWindow.show();
                    }
                }]
            }
        }]
    });
var descktop = Ext.create('widget.window', {
        id:'desk',
        height: '100%',
        width: '100%',
        title: 'Bars',
        closable: false,
        constrain: true,
        layout: 'fit',
        items:[panelStart],
        buttons: ['edit']
   }).show()
//panelStart.add(persons);
/*Ext.application({
    name: 'Desktop',

    //-------------------------------------------------------------------------
    // Most customizations should be made to Desktop.Application. If you need to
    // customize this file, doing so below this section reduces the likelihood
    // of merge conflicts when upgrading to new versions of Sencha Cmd.
    //-------------------------------------------------------------------------

    requires: [
        'static.Desktop.App'
    ],
    init: function() {
        var app = new Desktop.App();
    }
});*/


});

