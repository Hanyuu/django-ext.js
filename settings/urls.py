from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from apps.work.urls import urlpatterns as work_urls
from apps.register.urls import urlpatterns as registration_urls
from apps.api.urls import router
admin.autodiscover()

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
]

urlpatterns += work_urls
urlpatterns += router.urls
urlpatterns += registration_urls

if settings.DEBUG:
    urlpatterns += [
        url(r'', include('django.contrib.staticfiles.urls')),
    ]
