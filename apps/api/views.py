# -*- coding: utf-8 -*-
from rest_framework.response import Response
from django.http.response import HttpResponseForbidden,HttpResponseBadRequest,HttpResponse
from rest_framework import viewsets
from rest_framework.renderers import JSONRenderer
from rest_framework import status
from django.views import generic as cbv
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from .serializers import RegisterSerializer,PersonaSerializer
import traceback
import logging
import json

from apps.work.models import Register, Persona



logger = logging.getLogger(__name__)


class Api(viewsets.ModelViewSet):

    renderer_classes = [JSONRenderer,]
    permission_classes = []
    authentication_classes = []

class PersonaViewSet(Api):

    serializer_class = PersonaSerializer
    model = Persona

class RegistersViewSet(Api):

    serializer_class = RegisterSerializer
    model = Register

