# -*- coding: utf-8 -*-
import logging
import traceback
from datetime import datetime

from rest_framework import serializers
import anyjson

from apps.work.models import Register,Persona

logger = logging.getLogger(__name__)

class RegisterSerializer(serializers.ModelSerializer):

    class Meta:
        model = Register
        fields = ('id','name','inn','kpp')

class SimpleRegisterSerializer(serializers.ModelSerializer):

    class Meta:
        model = Register
        fields = ('name',)

class PersonaSerializer(serializers.ModelSerializer):

    register = serializers.SerializerMethodField('get_register')
    class Meta:
        model = Persona

        fields = ('id','name','last_name','birthday','register')

    def get_register(self,obj):
        return u'%s'%obj.get_register()