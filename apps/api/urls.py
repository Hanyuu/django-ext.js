# -*- coding: utf-8 -*-
from rest_framework import routers
from .views import PersonaViewSet,RegistersViewSet
router = routers.SimpleRouter()

router.register(r'^api/v1/registers', RegistersViewSet)
router.register(r'^api/v1/persons', PersonaViewSet)

