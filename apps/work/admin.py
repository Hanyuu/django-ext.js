# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Register, Persona

class RegisterAdmin(admin.ModelAdmin):
    list_display = ('id','name',)
    search_fields = ('name',)
admin.site.register(Register, RegisterAdmin)

class PersonaAdmin(admin.ModelAdmin):
    list_display = ('id','name','last_name','register',)
    search_fields = ('name','last_name',)
admin.site.register(Persona, PersonaAdmin)