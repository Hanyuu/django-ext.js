# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name='\u0418\u043c\u044f')),
                ('last_name', models.CharField(max_length=200, verbose_name='\u0418\u043c\u044f')),
                ('birthday', models.DateTimeField(verbose_name='\u0414\u0430\u0442\u0430 \u0440\u043e\u0436\u0434\u0435\u043d\u0438\u044f')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Register',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0443\u0447\u0435\u0440\u0435\u0436\u0434\u0435\u043d\u0438\u044f')),
                ('inn', models.IntegerField(verbose_name='\u0418\u041d\u041d')),
                ('kpp', models.IntegerField(verbose_name='\u041a\u041f\u041f')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='persona',
            name='register',
            field=models.ForeignKey(related_name='persons', to='work.Register'),
            preserve_default=True,
        ),
    ]
