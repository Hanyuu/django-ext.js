# -*- coding: utf-8 -*-
from django.views.generic import View
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from apps.work.helpers import cbv_decorator

@cbv_decorator(login_required(login_url='/register/'))
class IndexView(View):
    def get(self, request):
          return render_to_response('start.html',{}, RequestContext(request))
