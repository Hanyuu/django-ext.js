# -*- coding: utf-8 -*-
from django.db import models



class Register(models.Model):

    name = models.CharField(u'Название учереждения', max_length=200, blank=False, null=False)

    inn = models.IntegerField(u'ИНН',blank=False)

    kpp = models.IntegerField(u'КПП', blank=False)



class Persona(models.Model):

    name = models.CharField(u'Имя',max_length=200,blank=False,null=False)

    last_name = models.CharField(u'Имя',max_length=200,blank=False,null=False)

    birthday = models.DateTimeField(u'Дата рождения',null=False)

    register = models.ForeignKey(Register,related_name='persons')


    def get_register(self):
        return u'%s'%self.register.name