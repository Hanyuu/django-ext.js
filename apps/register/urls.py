# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from apps.register.views import RegistrationView

urlpatterns = [
    url(r'register/$', RegistrationView.as_view(), name='register'),
]