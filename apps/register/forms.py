# -*- coding: utf-8 -*-
import smtplib

from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


class RegistrationForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())
    register_or_login = forms.ChoiceField(choices=((0, u'Регистрация'), (1, u'Вход')), widget=forms.RadioSelect())

    def create_user(self):
        user = User(username=self.get_username_by_email(), email=self.cleaned_data['email'])
        user.set_password(self.cleaned_data['password'])
        user.save()
        return user

    def get_username_by_email(self):
        return self.cleaned_data['email'].split('@')[0]